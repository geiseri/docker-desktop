# Minimal Linux Desktop Docker

A base linux desktop that exposes itself over RDP.  It uses cloud-init
so it can be a transient base for building other desktops.  There is
no effort to offer persistence so that would be something that
would need to be added extra. The defaults have no user and no 
accessible root user.  Those would have to be turned on with cloud-init.  
Systemd is used so services can be enabled or disabled during cloud-init process too.  

By default it uses Openbox, tint2 and includes lxterm.  If there is a need
to add more packages or make larger changes the best approach
is to create a new Dockerfile and ``FROM geiseri/docker-desktop:latest``
and tweak it from there.  The base is from Debian 8.0 so check 
[debian testing](https://www.debian.org/releases/testing/).

# Configuration

Cconfigure the desktop uses cloud-init's
[No Cloud](http://cloudinit.readthedocs.org/en/latest/topics/datasources.html#no-cloud) 
data store by default.  Do configure you will need to create a directory with ``meta-data``
and ``user-data`` in it.

## meta-data:

```
instance-id: iid-local01
local-hostname: $HOSTNAME
```

## user-data:

```
#cloud-config
# vim: syntax=yaml

groups: 
  - developers
timezone: $TIMEZONE
locale: $LOCALE
users: 
  - gecos: "$FULLNAME"
    groups: 
      - users
    lock-passwd: false
    name: $USERNAME
    passwd: $PASSWORD_HASH

# required for NLA support in RDP
# write-files could also be used to include the same key
# for each instance
runcmd:
  - "openssl req -x509 -newkey rsa:2048 -nodes -keyout /etc/xrdp/key.pem -out /etc/xrdp/cert.pem -days 365 -subj '/CN=dockerdesktop'"
```

Currently the following modules are enabled:

* bootcmd
* write-files
* set_hostname
* update_hostname
* ca-certs
* users-groups
* ssh
* ssh-import-id
* set-passwords
* timezone
* runcmd
* scripts-vendor
* scripts-per-once
* scripts-per-boot
* scripts-per-instance
* scripts-user
* ssh-authkey-fingerprints
* keys-to-console
* phone-home
* final-message

**NOTE:** sshd installed but not enabled by default.  
[I agree with this assessment](http://jpetazzo.github.io/2014/06/23/docker-ssh-considered-evil/) 
but to each their own.



This docker uses systemd internally so the ``--cap-add SYS_ADMIN`` and ``-v /sys/fs/cgroup:/sys/fs/cgroup:ro`` are
required to let that work.  

```
docker run -dti --name=desktop --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v /path/to/cloud-init:/var/lib/cloud/seed/nocloud \
           -p 3389:3389 geiseri/docker-desktop
```

