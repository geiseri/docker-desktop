FROM debian:8

MAINTAINER "Ian Reinhart Geiser <geiseri@yahoo.com>"
ENV container docker

# update base image
RUN locale-gen --no-purge en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -yq update && \
    apt-get -yq install systemd \
                        pulseaudio pulseaudio-x11 \
                        cloud-init sudo openssh-client \
                        xauth ssh-askpass ttf-bitstream-vera \
                        ttf-unifont lxterminal tint2 hicolor-icon-theme \
                        openbox xserver-xorg wget


# install xrdp
RUN URL='http://bitbucket.com/geiseri/docker-desktop/my.deb'; FILE=$(mktemp); wget "$URL" -qO $FILE && sudo dpkg -i $FILE || true; rm $FILE && \
    apt-get -yqf install

# cleanup apt
RUN apt-get -yq autoremove && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN systemctl enable xrdp.service && \
    systemctl disable sshd.socket && \
    systemctl enable cloud-init-local.service && \
    systemctl enable cloud-init.service && \
    systemctl enable cloud-config.service && \
    systemctl enable cloud-final.service && \
    systemctl set-default multi-user.target
	
ADD systemd/systemd-tmpfiles-setup.service /etc/systemd/system/systemd-tmpfiles-setup.service.d/ignoresysfs.conf
ADD systemd/limits.conf /etc/security/limits.conf

# configure desktop
ADD openbox.rc.xml /etc/xdg/openbox/rc.xml
ADD tint2rc /etc/xdg/tint2/tint2rc
ADD cloud.cfg /etc/cloud/cloud.cfg

# Setup cloud-init
RUN mkdir -p /var/lib/cloud/seed/nocloud-net && \
    touch /var/lib/cloud/seed/nocloud-net/meta-data && \
    mkdir -p /var/lib/cloud/seed/nocloud

# Expose resources
VOLUME ["/sys/fs/cgroup","/var/lib/cloud/seed/nocloud"]
EXPOSE 3389

CMD ["lib/systemd/systemd"]

